const server  = require('coap').createServer();

const COAP_NOT_FOUND_CODE          = 404;
const COAP_METHOD_NOT_ALLOWED_CODE = 405;


server.on('request', function(request, response) {
    const resource = request.url.split('/')[1];
    console.log('Request received for resource ' + resource);
    
    if (request.method !== 'GET') {
        response.code = COAP_METHOD_NOT_ALLOWED_CODE;
        response.end();
        return;
    }
    
    switch (resource) {
        case 'hello':
			response.setOption('Content-Format', "text/plain");
            response.end('Hello');
			break;
        
        default:
            response.code = COAP_NOT_FOUND_CODE;
            response.end();
			break;
    }
})

server.listen(function() {
    console.log('Server running...');
})
